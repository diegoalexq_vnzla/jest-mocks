import { mount } from '@vue/test-utils'
import ZtRoot from '@/components/ZtRoot.vue'
import axios  from 'axios'
import MockAdapter from 'axios-mock-adapter'


const mock = new MockAdapter(axios);


describe('ZtRoot.vue', () => {
    const msg = 'New title'
    const wrapper = mount(ZtRoot, {
        propsData: {msg}
    })

    it('validamos si el componente recibe la propiedad msg ', () => {
        expect(wrapper.text()).toMatch(msg)
    })

    it('verificamos que todos los documentos inicien vacio ', () => {
        expect(wrapper.vm.$data.docs).toEqual([])
    })

    it("verificamos que exista un input", () => {
        expect(wrapper.find("input").exists()).toBeTruthy();
    });

    it("verificamos que exista un botón", () => {
        expect(wrapper.find("button").exists()).toBeTruthy();
    });

    it("Mostrar disabled el boton cuando el input este vacio", () => {
        // Cuando el input text esté vacío.
        wrapper.vm.$data.newDoc = ''
        // Aseguramos que el botón está deshabilitado.
        expect(wrapper.find("button").attributes("disabled")).toBeTruthy();
    });

    it('probando el axios',() => {
        mock.onGet('/users', { params: { searchText: 'Diego' } }).reply(200, {
            users: [
                { id: 1, name: 'Diego Quintero' }
            ]
        });
           
        axios.get('/users', { params: { searchText: 'Diego' } } )
        .then((response) => {
            console.log(response.data);
        })
        .catch((error) => {
            console.log(error.response);
        });
    })
})
