
    import axios from 'axios'
    import MockAdapter from 'axios-mock-adapter'

    const mock = new MockAdapter(axios)

    
    mock.onGet('/auth', { params: { user: 'diego', password:'1234' } }).reply(200, {
        users: [
            { id: 1, name: 'Diego',lastname:'Quintero' }
        ]
    })

    mock.onGet('/auth', { params: { user: 'asdasd', password:'asdasd' } }).reply(404, {
        error: "Usuario inexistente"
    })

    mock.onGet('/auth', { params: { user: 'kevtest', password:'1234' } }).reply(404, {
        error: "Usuario bloqueado"
    })